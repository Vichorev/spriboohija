package com.spriboohija.demo;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;
    
    @GetMapping
    public List<User> all() {
        return userRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> byId(@PathVariable("id") Long id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent())
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(user.get());
    }

    @PostMapping
	public ResponseEntity<Object> add(@RequestBody User user) {
        User saved = userRepository.save(user);

	    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
		    	.buildAndExpand(saved.getId()).toUri();

	    return ResponseEntity.created(location).build();
    }
    
    @PutMapping("/{id}")
	public ResponseEntity<Object> update(@PathVariable("id") Long id, @RequestBody User user) {
	    if (!userRepository.existsById(id))
		    return ResponseEntity.notFound().build();

	    user.setId(id);
	
	    userRepository.save(user);

	    return ResponseEntity.noContent().build();
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteStudent(@PathVariable("id") long id) {
        if (!userRepository.existsById(id))
            return ResponseEntity.notFound().build();
        
        userRepository.deleteById(id);

        return ResponseEntity.status(204).build();
    }

}